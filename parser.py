#DataAnalysis
import pandas as pd
#DataBase ORM
from peewee import *

import os
from datetime import datetime
import math

db = SqliteDatabase('tir_data.db')
db.connect()

class TimeStampField(Field):
    """ Field for representing full timestamp in Sqlite

        Timestamp is stored in db in format:
            %Y-%m-%d %H:%M:%S%z
        in example:
            2019-11-10 16:22:58+01:00

        Timestamp passed to database must be in format:
            %d.%m.%Y-%H:%M:%S%z
        in example:
            10.11.19-16:22:58+0100                      """

    field_type = 'str'

    def db_value(self, value):
        return str(value)

    def python_value(self, value):
        value = value.split(':')
        date = ':'.join(value[:-1]) + ''.join(value[-1:])
        return datetime.strptime(date, "%Y-%m-%d %H:%M:%S%z")

    def timestamp_from_parts(date, time, timezone):
        timezone = ''.join(timezone.split(':'))
        timestamp = f"{date}-{time}{timezone}"
        return datetime.strptime(timestamp, "%d.%m.%Y-%H:%M:%S%z")


class Refuel(Model):
    """ Refueling record """
    timestamp = TimeStampField()
    car_id = CharField()
    driver = CharField()
    country = CharField()
    address = CharField()
    brand = CharField()
    cat = CharField()
    station = CharField()
    price = FloatField()
    quantity = IntegerField()
    paid = FloatField()
    currency = CharField()
    service = CharField()
    reciept = IntegerField()
    insertion_date = DateField()

    class Meta:
        database = db


db.create_tables([Refuel])


def load_file(filename):
    """ Load an excel file to pandas' dataframe from assets directory of this binary """
    dir = os.path.dirname(__file__)
    return pd.read_excel(
            os.path.join(dir, 'assets', filename),
            id_column=0)


def translate_key(key):
    """ Returns database name of an Excel key """
    translator = {
        'Gmt':              'timezone',
        'Kraj':             'country',
        'Data':             'date',
        'Czas':             'time',
        'Cena':             'price',
        'Adres':            'address',
        'Brand':            'brand',
        'Ilość':            'quantity',
        'Kwota':            'paid',
        'Stacja':           'station',
        'Waluta':           'currency',
        'Usługa':           'service',
        'Paragon':          'reciept',
        'Kierowca':         'driver',
        'Kategoria':        'cat',
        'Data dodania':     'insertion_date',
        'Numer samochodu':  'car_id',
    }
    return translator[key]


def from_pd_series(series):
    record = { translate_key(k): v for k, v in series.items() }
    timestamp = TimeStampField.timestamp_from_parts(record['date'],
                                                    record['time'],
                                                    record['timezone'])
    for x in ['date', 'time', 'timezone']:
        record.pop(x)

    record['timestamp'] = timestamp

    if math.isnan(record['reciept']):
        record['reciept'] = 0

    for k, v in record.items():
        if k == 'reciept':
            if math.isnan(v):
                record[k] = 0
            record[k] = int(record[k])
        elif k in ['price', 'paid']:
            record[k] = float(record[k])
        try:
            if math == 'nan' or math.isnan(v):
                record[k] = 'n/a'
        except:
            pass
    return record


def parse_file(filename='transaction2.xls'):
    file = load_file(filename)
    frame = []
    for _, series in file.iterrows():
        frame.append(from_pd_series(series))
    return frame


def already_exists(record):
    """ Check if a record already exists in a database """
    doppler = Refuel.get_or_none(
            (Refuel.timestamp == record['timestamp']) &
            (Refuel.paid == record['paid']))
    return doppler is not None


def update_db(file='transaction2.xls'):
    """ Update database with records from file """
    file = load_file(file)
    data = parse_file(filename=file)
    data = [record for record in data if not already_exists(record)]
    print(data)
    with db.atomic():
        Refuel.insert_many(data).execute()
